FROM ubuntu:19.04

LABEL author="mathieu auclair"
LABEL email="mathieu.auclair@ubisoft"
LABEL version="1.0.0"

ENV DOTNET_VERSION="3.1.0"

RUN apt-get update

RUN apt-get install apt-utils -y
RUN apt-get install wget -y

RUN wget -q https://packages.microsoft.com/config/ubuntu/19.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
RUN dpkg -i packages-microsoft-prod.deb

RUN apt-get update
RUN apt-get install aspnetcore-runtime-2.2 dotnet-runtime-2.2 -y
RUN apt-get install apt-transport-https -y
RUN apt-get update
RUN apt-get install dotnet-sdk-2.2 -y

